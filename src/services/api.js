import axios from 'axios';

// axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
const api = axios.create({
  baseURL: 'https://api.github.com',
});

export default api;
