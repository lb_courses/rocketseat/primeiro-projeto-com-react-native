import Reactotron from 'reactotron-react-native';

// em ambiante de dev
if (__DEV__) {
  const tron = Reactotron.configure({ host: '192.168.11.9' })
    .useReactNative()
    .connect();

  console.tron = tron;
  tron.clear();
}
