import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Main from './pages/Main';
import User from './pages/User';
import Repository from './pages/Repository';

const Routes = createAppContainer(
  createStackNavigator(
    {
      Main,
      User,
      Repository,
    },
    {
      // coloca o título no centro
      headerLayoutPreset: 'center',
      // deixa apenas o icone de voltar, remove o texto da page anterior
      headerBackTitleVisible: false,
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: '#7159c1',
        },
        // cor dos componentes dentro do header
        headerTintColor: '#FFF',
      },
    }
  )
);

export default Routes;
