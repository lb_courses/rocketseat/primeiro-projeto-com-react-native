import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ActivityIndicator } from 'react-native';

import api from '../../services/api';
import {
  Container,
  Header,
  Avatar,
  Name,
  Bio,
  Stars,
  Starred,
  OwnerAvatar,
  Info,
  Title,
  Author,
} from './styles';
// export default function User({ navigation }) {
export default class User extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('user').name,
  });

  state = {
    stars: [],
    loading: true,
    page: 1,
    refreshing: false,
  };

  async componentDidMount() {
    this.loadDate();
  }

  // eslint-disable-next-line react/sort-comp
  async loadDate(page = 1) {
    console.tron.log('loadDate');
    const { navigation } = this.props;
    const user = navigation.getParam('user');
    const { stars } = this.state;
    this.setState({ loading: true, refreshing: false });
    const response = await api.get(`/users/${user.login}/starred?page=${page}`);
    this.setState({
      stars: page >= 2 ? [...stars, ...response.data] : response.data,
      loading: false,
    });
  }

  async loadMore() {
    const { page } = this.state;
    const next = page + 1;
    this.loadDate(next);
  }

  refreshList = async () => {
    await this.setState({ refreshing: true, stars: [], page: 1 });
    this.loadDate();
  };

  handleNavigate = repository => {
    console.tron.log('ok');
    const { navigation } = this.props;
    // eslint-disable-next-line react/prop-types
    navigation.navigate('Repository', { repository });
  };

  render() {
    const { navigation } = this.props;
    const { stars, loading, refreshing } = this.state;
    const user = navigation.getParam('user');
    return (
      <Container>
        <Header>
          <Avatar source={{ uri: user.avatar }} />
          <Name>{user.name}</Name>
          <Bio>{user.bio}</Bio>
        </Header>

        <Stars
          data={stars}
          onRefresh={() => this.refreshList()} // Função dispara quando o usuário arrasta a lista pra baixo
          refreshing={refreshing} // Variável que armazena um estado true/false que representa se a lista está atualizando
          onEndReachedThreshold={0.2} // Carrega mais itens quando chegar em 20% do fim
          onEndReached={() => this.loadMore()} // Função que carrega mais itens
          keyExtractor={star => String(star.id)}
          renderItem={({ item }) => (
            <Starred onPress={() => this.handleNavigate(item)}>
              <OwnerAvatar source={{ uri: item.owner.avatar_url }} />
              <Info>
                <Title>{item.name}</Title>
                <Author>{item.owner.login}</Author>
              </Info>
            </Starred>
          )}
        />

        {loading ? <ActivityIndicator color="#bc2b78" size="large" /> : <></>}
      </Container>
    );
  }
}

User.propTypes = {
  navigation: PropTypes.shape({
    getParam: PropTypes.func,
  }).isRequired,
};
