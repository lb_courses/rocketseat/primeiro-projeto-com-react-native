/**
 * @format
 */

// yarn add eslint -D
// yarn eslint --init
// yarn add prettier eslint-config-prettier eslint-plugin-prettier babel-eslint  -D
// yarn add reactotron-react-native
// yarn add react-navigation
// yarn add react-native-reanimated react-native-gesture-handler react-native-screens@^1.0.0-alpha.23
// yarn add react-navigation-stack
// yarn add styled-components
// yarn add react-native-vector-icons
// yarn add axios
// yarn add @react-native-community/async-storage
// yarn add prop-types
// yarn add react-native-webview
import { AppRegistry } from 'react-native';
import App from './src';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => App);
